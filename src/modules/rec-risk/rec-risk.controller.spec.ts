import { Test, TestingModule } from '@nestjs/testing';
import { RecRiskController } from './rec-risk.controller';

describe('RecRiskController', () => {
  let controller: RecRiskController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecRiskController],
    }).compile();

    controller = module.get<RecRiskController>(RecRiskController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
