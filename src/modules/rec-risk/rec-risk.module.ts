import { Module } from '@nestjs/common';
import { RecRiskController } from './rec-risk.controller';
import { RecRiskService } from './rec-risk.service';

@Module({
  controllers: [RecRiskController],
  providers: [RecRiskService],
})
export class RecRiskModule {}
