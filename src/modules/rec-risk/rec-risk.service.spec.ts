import { Test, TestingModule } from '@nestjs/testing';
import { RecRiskService } from './rec-risk.service';

describe('RecRiskService', () => {
  let service: RecRiskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecRiskService],
    }).compile();

    service = module.get<RecRiskService>(RecRiskService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
