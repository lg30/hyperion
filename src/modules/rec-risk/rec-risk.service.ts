import { Injectable } from '@nestjs/common';
import { getMongoManager } from 'typeorm';
import {
  RecRiskGetReqQueryDTO,
  RecRiskGetResBody_DTO,
  RecRisk_DTO,
} from './dto';
import { Branches } from '../../entities/branches';
import { SlipsPremium } from '../../entities/slips_premium';
import { GroupsCedants } from '../../entities/groups_cedants';
import { Cedants } from '../../entities/cedants';
import { Countries } from '../../entities/countries';
import { CaseNotLifePremium } from '../../entities/case_not_life_premium';
import { Region } from '../../entities/region';
import { CedantTypes } from '../../entities/cedant_types';
import { ObjectID } from 'bson';

@Injectable()
export class RecRiskService {
  public async get(
    query: RecRiskGetReqQueryDTO,
  ): Promise<RecRiskGetResBody_DTO> {
    const manager = getMongoManager();

    const [regions, countries, groupsCedants, mainBranches, cedantTypes] =
      await Promise.all([
        manager.find(Region, {
          where: {
            ...(query.zones && {
              name: { $in: query.zones },
            }),
          },
        }),
        manager.find(Countries, {
          where: {
            ...(query.countries && {
              name: { $in: query.countries },
            }),
          },
        }),
        manager.find(GroupsCedants, {
          where: {
            ...(query.groups && {
              name: { $in: query.groups },
            }),
          },
        }),
        manager.find(Branches, {
          where: {
            ...(query.mainBranches && {
              name: { $in: query.mainBranches },
            }),
            is_parent: 1, // this means that it's a main branch
          },
        }),
        manager.find(CedantTypes, {
          where: {
            ...(query.cedantTypes && {
              name: { $in: query.cedantTypes },
            }),
          },
        }),
      ]);

    const cedants = await manager.find(Cedants, {
      where: {
        ...(query.zones && {
          region_id: {
            $in: regions.map((region) => new ObjectID(region._id.toString())),
          },
        }),
        ...(query.companies && {
          name: { $in: query.companies },
        }),
        ...(query.groups && {
          groups_cedants_id: {
            $in: groupsCedants.map(
              (groupCedant) => new ObjectID(groupCedant._id.toString()),
            ),
          },
        }),
        ...(query.countries && {
          countries_id: {
            $in: countries.map(
              (country) => new ObjectID(country._id.toString()),
            ),
          },
        }),
        ...(query.cedantTypes && {
          types_cedants_id: {
            $in: cedantTypes.map(
              (cedantType) => new ObjectID(cedantType._id.toString()),
            ),
          },
        }),
      },
    });

    const slipsPremiums = await manager.find(SlipsPremium, {
      where: {
        ...(query.validationStatuses && {
          validation_status: { $in: query.validationStatuses },
        }),
        ...(query.confirmationStatuses && {
          confirmation_status: { $in: query.confirmationStatuses },
        }),
        ...(query.publishedDate && {
          published_date: query.publishedDate,
        }),
        ...(query.editedPeriod && {
          edited_period: query.editedPeriod,
        }),
        cedants_id: {
          $in: cedants.map((cedant) => new ObjectID(cedant._id.toString())),
        },
      },
    });

    const totalPremiumHt = await manager.aggregate(CaseNotLifePremium, [
      {
        $match: {
          cedants_id: {
            $in: cedants.map((cedant) => new ObjectID(cedant._id.toString())),
          },
          slipes_prime_id: {
            $in: slipsPremiums.map(
              (slipsPremium) => new ObjectID(slipsPremium._id.toString()),
            ),
          },
          branches_id: {
            $in: mainBranches.map(
              (branch) => new ObjectID(branch._id.toString()),
            ),
          },
        },
      },
      {
        $group: {
          _id: {
            cedants_id: '$cedants_id',
            slipes_prime_id: '$slipes_prime_id',
            branches_id: '$branches_id',
          },
          total_premium_ht: { $sum: '$premium_ht' },
        },
      },
      {
        $sort: {
          _id: 1,
        },
      },
      {
        $limit: query.limit !== undefined ? Number(query.limit) : 100,
      },
      {
        $skip: query.offset !== undefined ? Number(query.offset) : 0,
      },
    ]);

    const data = await totalPremiumHt.toArray();

    const recRisks: RecRisk_DTO[] = [];
    for (const item of data) {
      const resItem: RecRisk_DTO = new RecRisk_DTO();

      const slipsPremium = slipsPremiums.find(
        (v) => v._id.toString() === item._id.slipes_prime_id.toString(),
      );
      const cedant = cedants.find(
        (v) => v._id.toString() === item._id.cedants_id.toString(),
      );
      const branch = mainBranches.find(
        (v) => v._id.toString() === item._id.branches_id.toString(),
      );

      // reference
      resItem.reference = slipsPremium.reference;

      // country
      resItem.country = countries.find(
        (v) => v._id.toString() === cedant.countries_id.toString(),
      ).name;

      // cedants
      resItem.cedants = cedant.name;

      // validation_status
      resItem.validation_status = slipsPremium.validation_status;

      // confirmation_status
      resItem.confirmation_status = slipsPremium.confirmation_status;

      // publish_date
      resItem.publish_date = new Date(slipsPremium.published_date);

      // branche
      resItem.branche = branch.name;

      // calculated_REC
      resItem.calculated_REC = item.total_premium_ht * 0.36;

      recRisks.push(resItem);
    }

    return {
      recRisks,
    };
  }
}
