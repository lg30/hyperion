export class RecRiskGetReqQueryDTO {
  zones!: undefined | Array<string>;
  companies!: undefined | Array<string>;
  editedPeriod!: undefined | Date;
  countries!: undefined | Array<string>;
  validationStatuses!: undefined | Array<string>;
  mainBranches!: undefined | Array<string>;
  groups!: undefined | Array<string>;
  confirmationStatuses!: undefined | Array<string>;
  dateCutOff!: undefined | Date;
  cedantTypes!: undefined | Array<string>;
  publishedDate!: undefined | Date;
  method!: undefined | string;
  limit!: undefined | number;
  offset!: undefined | number;
}
