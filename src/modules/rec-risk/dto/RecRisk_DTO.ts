export class RecRisk_DTO {
  reference!: string;
  country!: string;
  cedants!: string;
  validation_status!: string;
  confirmation_status!: string;
  publish_date!: Date;
  branche!: string;
  calculated_REC!: number;
}
