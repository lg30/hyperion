import { RecRisk_DTO } from './RecRisk_DTO';

export class RecRiskGetResBody_DTO {
  recRisks: Array<RecRisk_DTO>;
}
