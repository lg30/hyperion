import { Controller, Get, Query } from '@nestjs/common';
import { RecRiskService } from './rec-risk.service';
import { RecRiskGetReqQueryDTO, RecRiskGetResBody_DTO } from './dto';

@Controller('rec-risk')
export class RecRiskController {
  public constructor(private recRiskSvc: RecRiskService) {}

  @Get()
  async get(
    @Query() query: RecRiskGetReqQueryDTO,
  ): Promise<RecRiskGetResBody_DTO> {
    return await this.recRiskSvc.get(query);
  }
}
