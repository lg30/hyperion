import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class CedantTypes {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  name: string;
}
