import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Branches {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  alias: Array<string>;

  @Column()
  code: string;

  @Column()
  is_parent: number;

  @Column()
  name: string;

  @Column({ type: 'timestamptz', nullable: false })
  created_at!: Date;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at!: Date;

  @Column({ type: 'timestamptz', nullable: true })
  deleted_at!: null | Date;

  @ObjectIdColumn({ name: 'parent_branch_id' })
  parent_branch_id: ObjectID;

  @Column()
  status: number;

  @Column()
  type: string;
}
