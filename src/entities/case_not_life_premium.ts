import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class CaseNotLifePremium {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  active_status: number;

  @Column()
  branch: string;

  @ObjectIdColumn({ name: 'cedants_id' })
  cedants_id: ObjectID;

  @Column()
  comission_cession: number;

  @Column()
  date_effective!: string;

  @Column()
  insured_capital!: number;

  @Column()
  invoiced_premium!: string;

  @ObjectIdColumn({ name: 'nature_risque_id' })
  nature_risque_id!: ObjectID;

  @Column()
  paid_commission!: number;

  @Column()
  part_cedant_coass!: number;

  @ObjectIdColumn({ name: 'slipes_prime_id' })
  slipes_prime_id!: ObjectID;

  @Column()
  sub_branches_id!: number;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at!: Date;

  @ObjectIdColumn({ name: 'branches_id' })
  branches_id: ObjectID;

  @Column()
  commission_refunded: string;

  @Column()
  date_transaction: string;

  @Column()
  net_amount: string;

  @Column()
  policy_number: string;

  @Column()
  case_validation_status: string;

  @Column()
  deadline: string;

  @Column()
  premium_ceded: number;

  @Column()
  case_variant: string;

  @Column()
  premium_ht: number;

  @Column()
  category: string;

  @Column()
  prime_net_ceded: number;
}
