import { Entity, Column, ObjectIdColumn, ObjectID, ObjectType } from 'typeorm';

@Entity()
export class SlipsPremium {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  approval_status: string;

  @ObjectIdColumn({ name: 'cedants_id' })
  cedants_id: ObjectID;

  @ObjectIdColumn({ name: 'cedants_type_id' })
  cedants_type_id: ObjectID;

  @Column()
  comission_refunded_total: number;

  @Column({ type: 'timestamptz', nullable: false })
  created_at!: Date;

  @Column()
  edited_period!: string;

  @Column()
  file_url!: string;

  @Column()
  invoice_premium_total!: string;

  @Column()
  is_paid!: number;

  @Column()
  net_amount_total: string;

  @Column()
  published_date: string;

  @ObjectIdColumn({ name: 'reinsurances_id' })
  reinsurances_id: ObjectID;

  @Column()
  slip_type: string;

  @Column()
  update_progress: number;

  @ObjectIdColumn({ name: 'user_cedant_id' })
  user_cedant_id: ObjectID;

  @Column()
  confirmation_date: string;

  @Column()
  reference: string;

  @Column()
  update_status: number;

  @Column({ type: 'timestamptz', nullable: false })
  validation_date: Date;

  @Column()
  confirmation_status: string;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at: Date;

  @Column()
  validation_status: string;

  @Column()
  warnings: ObjectType<any>; // ?

  @Column()
  warnings_saved: number;
}
