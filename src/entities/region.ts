import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Region {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  code: string;

  @Column({ type: 'timestamptz', nullable: false })
  created_at!: Date;

  @Column()
  name: string;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at!: Date;
}
