import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Cedants {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  benefit_percentage: string;

  @Column()
  code: string;

  @Column()
  color1: string;

  @Column()
  contact: string;

  @Column({ type: 'timestamptz', nullable: false })
  created_at!: Date;

  @ObjectIdColumn({ name: 'currencies_id' })
  currencies_id!: ObjectID;

  @Column()
  email!: string;

  @ObjectIdColumn({ name: 'groups_cedants_id' })
  groups_cedants_id!: ObjectID;

  @Column()
  logo!: string;

  @Column()
  name!: string;

  @ObjectIdColumn({ name: 'region_id' })
  region_id!: ObjectID;

  @ObjectIdColumn({ name: 'reinsurances_id' })
  reinsurances_id!: ObjectID;

  @ObjectIdColumn({ name: 'types_cedants_id' })
  types_cedants_id!: ObjectID;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at!: Date;

  @Column()
  color2: string;

  @ObjectIdColumn({ name: 'countries_id' })
  countries_id: ObjectID;

  @Column()
  estimation_type: string;
}
