import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Countries {
  @ObjectIdColumn({ name: '_id' })
  _id: ObjectID;

  @Column()
  code: string;

  @Column({ type: 'timestamptz', nullable: false })
  created_at!: Date;

  @Column({ type: 'timestamptz', nullable: false })
  deleted_at!: Date;

  @Column()
  name: string;

  @ObjectIdColumn({ name: 'regions_id' })
  regions_id!: ObjectID;

  @Column({ type: 'timestamptz', nullable: false })
  updated_at!: Date;
}
