import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecRiskController } from './modules/rec-risk/rec-risk.controller';
import { RecRiskService } from './modules/rec-risk/rec-risk.service';
import { Branches } from './entities/branches';
import { CaseNotLifePremium } from './entities/case_not_life_premium';
import { Cedants } from './entities/cedants';
import { Countries } from './entities/countries';
import { GroupsCedants } from './entities/groups_cedants';
import { Region } from './entities/region';
import { SlipsPremium } from './entities/slips_premium';
import { CedantTypes } from './entities/cedant_types';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'hyperion',
      entities: [
        Branches,
        CaseNotLifePremium,
        Cedants,
        Countries,
        GroupsCedants,
        Region,
        SlipsPremium,
        CedantTypes,
      ],
      synchronize: true,
    }),
  ],
  controllers: [RecRiskController],
  providers: [RecRiskService],
})
export class AppModule {}
