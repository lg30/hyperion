## Installation

```bash
$ yarn install
```

## Running the app

```bash
$ yarn start
```

## Endpoint for test

The endpoint for checking the task is http://localhost:8000/rec-risk. You should send a GET request. Please check Req Query DTO for available filters.
